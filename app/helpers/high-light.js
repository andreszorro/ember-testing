import Ember from 'ember';

export function highLight(params) {
  let found = new RegExp('(' + params[1] + ')', 'gi'),
      v = Ember.Handlebars.Utils.escapeExpression(params[0]);

  return Ember.String.htmlSafe(v.replace(found, '<strong>$1</strong>'));
}

export default Ember.Helper.helper(highLight);
