import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('index', {
    path: '/'
  });
  this.route('about', {});
  this.route('users', {
    path: '/people'
  }, function() {
    this.route('user', {
      path: '/:id'
    });
  });
  this.route('search', {
    path: '/search/:searchTerm'
  });
  this.route('profile', {}, function() {
    this.route('new', {});
    this.route('user', {
      path: '/:id'
    });
    this.route('login', {});
  });
});

export default Router;
