import Ember from 'ember';
import stringFormat from 'embertesting/utils/string-format';

const DEFAULT_RULES = {
  required: {
    msg: 'Property "{0}" is required.',
    fn(v) {
      return Boolean(v);
    }
  },
  regex: {
    msg: 'Property "{0}" must match "{1}".',
    fn(v, regex) {
      if (v && regex instanceof RegExp) {
        return regex.test(v);
      }
    }
  },
  string: {
    msg: 'Property "{0}" must be a string.',
    fn(v) {
      return typeof v === 'string' && isNaN(v);
    }
  },
  number: {
    msg: 'Property "{0}" must be a number.',
    fn(v) {
      return !isNaN(v);
    }
  },
  email: {
    msg: 'Property "{0}" must be a valid email.',
    fn(v) {
      return /^[a-zA-Z\-0-9\.]+@[a-zA-Z\-0-9\.]+\.\w{2,3}$/.test(v);
    }
  },
  url: {
    msg: 'Property "{0}" must be a valid url.',
    fn(v) {
      return /^https?:\/\/.*/.test(v);
    }
  },
  maxlength: {
    msg: 'Property "{0}" length must not exceed {1} character(s).',
    fn(v, len) {
      let num = typeof v === 'number' ? v : (v && v.hasOwnProperty('length') ? v.length : null);
      if (num) {
        return num && num <= Number(len);
      }
    }
  },
  minlength: {
    msg: 'Property "{0}" must have at least {1} character(s).',
    fn(v, len) {
      let num = typeof v === 'number' ? v : (v && v.hasOwnProperty('length') ? v.length : null);
      if (num) {
        return num >= Number(len);
      }
    }
  }
};

let VALIDATIONS = Object.assign({}, DEFAULT_RULES);

/**
 * Runs validation
 * @private
 */
function _runValidation(value, validation, opts) {
  let fn;

  if (!VALIDATIONS[validation]) {
    throw `ValidationsError: No valid rule applies for "${validation}"`;
  }

  fn = VALIDATIONS[validation].fn;

  if (fn && fn.call(this, value, opts) === false) {
    return VALIDATIONS[validation].msg;
  }
}

/**
 * Adds validation
 */
export function addValidation(name, msg, fn) {
  if (!DEFAULT_RULES.string.fn(name) ||
    !DEFAULT_RULES.string.fn(msg) ||
    typeof fn !== 'function') {
    return;
  }

  VALIDATIONS[name] = { msg, fn };
}

/**
 * Edits validation message
 */
export function editMsg(name, msg) {
  if (VALIDATIONS[name] && DEFAULT_RULES.string.fn(msg)) {
    VALIDATIONS[name].msg = msg;
  }
}

/**
 * Edits validation function
 */
export function editValidation(name, fn) {
  if (VALIDATIONS[name] && typeof fn !== 'function') {
    VALIDATIONS[name].fn = fn;
  }
}

export default Ember.Mixin.create({
  /**
   * Runs all validation rules
   */
  validate() {
    var model = this.toJSON(),
      rules = this.__validations,
      results = {};

    for (let key of Object.keys(rules)) {
      results[key] = {};
      for (let rule of Object.keys(rules[key])) {
        if (rules[rule] !== null) {
          let r = _runValidation(model[key], rule, rules[key][rule]);

          if (r) {
            results[key][rule] = stringFormat(r, key, rules[key][rule]);
          }

        }
      }

      if (!Object.keys(results[key]).length) {
        delete results[key];
      }
    }

    return Object.keys(results).length ? results : false;
  }
});
