import DS from 'ember-data';

export default DS.Model.extend({
	body:  DS.attr('string'),
	user: DS.belongsTo('user'),
	createdAt: DS.attr('string', {
    defaultValue: function() { return new Date(); }
  })
});
