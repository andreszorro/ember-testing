import DS from 'ember-data';
import {default as v, editMsg} from 'embertesting/mixins/validations';

editMsg('regex', 'Property "{0}" must contain only alpha-numeric characters.');

export default DS.Model.extend(v, {
  name: DS.attr('string'),
  username: DS.attr('string'),
  email: DS.attr('string'),
  address: DS.attr('string'),
  // address: {
  //   street: DS.attr('string'),
  //   suite: DS.attr('string'),
  //   city: DS.attr('string'),
  //   zipcode: DS.attr('string'),
  //   geo: {
  //     lat: DS.attr('string'),
  //     long: DS.attr('string')
  //   }
  // },
  phone: DS.attr('string'),
  website: DS.attr('string'),
  company: DS.attr('string'),
  posts: DS.hasMany('post'),

  // Validations
  __validations: {
    name: {
      required: true,
      string: true
    },
    username: {
      required: true,
      string: true,
      maxlength: 20,
      minlength: 3,
      regex: /^[a-zA-Z0-9]+$/g
    },
    email: {
      email: true
    },
    website: {
      url: true
    }
  }
});
