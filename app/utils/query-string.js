export function stringify(query) {
  return Object
    .keys(query)
    .map(q => q + '=' + query[q])
    .join('&');
}

export function parse(qs) {
  let obj = {};
  qs
    .split('&')
    .forEach(str => {
      str = str.split('=');
      obj[str[0]] = str[1];
    });

  return obj;
}
