/**
 * Formats string in defined manner
 * @param {string} v      The string to parse
 * @param {array}  terms  Terms to replace
 */
export default function stringFormat(v, ...terms) {
  return v.replace(/{(\d+)}/g, (match, num) => {
    let term = terms[num];

    if (typeof term === 'object') {
      term = term.toString();
    }

    return typeof term !== 'undefined' ? term : match;
  });
}
