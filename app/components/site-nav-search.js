import Ember from 'ember';

export default Ember.Component.extend({
  isInvalid: false,
  actions: {
    search() {
      let param = this.get('searchParam');
      if (!param) {
        this.set('isInvalid', true);
      } else {
        this.set('isInvalid', false);
        return this.sendAction();
      }
    }
  }
});
