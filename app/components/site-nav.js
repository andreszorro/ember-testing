import Ember from 'ember';

export default Ember.Component.extend({
  searchParam: '',
  navItems: Ember.inject.service(),
  nav: null,
  loadNav: Ember.on('didInsertElement', function() {
    this.get('navItems').getNav().then(nav => {
      this.set('nav', nav);
    });
  }),
  actions: {
    search() {
      this.sendAction('action', this.get('searchParam'));
    }
  }
});
