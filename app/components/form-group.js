import Ember from 'ember';

export default Ember.Component.extend({
  errorMsg: Ember.computed('hasError', 'customMsg', function() {
    return Object.assign(
      this.get('hasError'),
      this.get('customMsg') || {}
    );
  })
});
