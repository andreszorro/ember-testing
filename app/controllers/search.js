import Ember from 'ember';

export default Ember.Controller.extend({
  showModal: false,
  actions: {
    toggleModal(user) {
      Ember.Logger.log('Toggled: ', this.get('showModal'));
      this.set('currentUser', user);
      this.toggleProperty('showModal');
    }
  }
});
