import Ember from 'ember';

export default Ember.Controller.extend({
  newPostBody: '',
	submitAction(postText) {
		let model = this.model;
    let newPost = this.store.createRecord('post', {
      user: model,
      body: postText
    });

		model
			.get('posts')
			.then(posts => {
				posts.pushObject(newPost);

        // Return promise to chain
        return model.save();
			}).then(() => this.set('newPostBody', ''));
	}
});
