export function initialize(appInstance) {
	console.log('Hola mundo!');
	var store = appInstance.lookup('service:store');

  store.findAll('user').then((users) => {
    let len = users.get('length');
     // It gets anoying with so many John Does
     if (len < 5) {
        store.createRecord('user', {
          name: 'John Doe ' + len,
          username: 'johndoe' + len,
          email: 'johndoe@gmail.com',
          address: 'Av. Jhon Doe 1638',
          phone: '+55johndoe',
          website: 'johndoe.com',
          company: 'Jhon Doe Inc'
        }).save();
     }
  });

	/*store.push({
		data: [{
			id: 1,
			type: 'user',
			attributes:{
				name: 'Jhon Doe',
				username: 'johndoe',
				email: 'johndoe@gmail.com',
				address: 'Av. Jhon Doe 1638',
				phone: '+55johndoe',
				website: 'johndoe.com',
				company: 'Jhon Doe Inc'
			},
			relationships: {}
		}]

	})
	//console.log(store.push);*/
}

export default {
    name: 'seed',
    initialize: initialize
};
