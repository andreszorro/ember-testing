import DS from 'ember-data';
import Ember from 'ember';

export default DS.LSAdapter.extend({
  //namespace: 'dummy-sn',
  loadRelationships: function(store, type, record) {
    var adapter = this,
        // resultJSON = {},
        // modelName = type.modelName,
        relationshipNames, relationships;
        // relationshipPromises = [];

    /**
     * Create a chain of promises, so the relationships are
     * loaded sequentially.  Think of the variable
     * `recordPromise` as of the accumulator in a left fold.
     */
    var recordPromise = Ember.RSVP.resolve(record);

    relationshipNames = Ember.get(type, 'relationshipNames');
    relationships = relationshipNames.belongsTo
      .concat(relationshipNames.hasMany);

    relationships.forEach(function(relationName) {
      var relationModel = type.typeForRelationship(relationName,store);
      var relationEmbeddedId = record[relationName];
      var relationProp  = adapter.relationshipProperties(type, relationName);
      var relationType  = relationProp.kind;
      // var foreignAdapter = store.adapterFor(relationModel.modelName);

      var opts = {allowRecursive: false};

      /**
       * embeddedIds are ids of relations that are included in the main
       * payload, such as:
       *
       * {
       *    cart: {
       *      id: "s85fb",
       *      customer: "rld9u"
       *    }
       * }
       *
       * In this case, cart belongsTo customer and its id is present in the
       * main payload. We find each of these records and add them to _embedded.
       */
      if (relationEmbeddedId && DS.LSAdapter.prototype.isPrototypeOf(adapter))
      {
        recordPromise = recordPromise.then(function(recordPayload) {
          var promise;
          if (typeof record[relationName][0] === 'object') {
            promise = Ember.RSVP.resolve(record[relationName]);
          } else if (relationType === 'belongsTo' || relationType === 'hasOne') {
            promise = adapter.findRecord(null, relationModel, relationEmbeddedId, opts);
          } else if (relationType === 'hasMany') {
            promise = adapter.findMany(null, relationModel, relationEmbeddedId, opts);
          }

          return promise.then(function(relationRecord) {
            return adapter.addEmbeddedPayload(recordPayload, relationName, relationRecord);
          });
        });
      }
    });

    return recordPromise;
  },
  queryRecord: function (store, type, query/*, recordArray*/) {
    var namespace = this._namespaceForType(type);
    var result = this._queryRecord(namespace.records, query);

    if (result && result.id) {
      return this.loadRelationships(store, type, result);
    } else {
      return Ember.RSVP.reject();
    }
  },

  _queryRecord: function (records, query) {
    var record;

    function recordMatchesQuery(record) {
      return Object.keys(query).every(function(property) {
        var test = query[property];
        if (Object.prototype.toString.call(test) === '[object RegExp]') {
          return test.test(record[property]);
        } else {
          return record[property] === test;
        }
      });
    }

    for (var id in records) {
      record = records[id];
      if (recordMatchesQuery(record)) {
        return Ember.copy(record);
      }
    }
    return null;
  }
});

/*import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  host: 'http://jsonplaceholder.typicode.com',
  findAll(store, model) {
    return fetch(this.host + '/' + model.modelName + 's')
      .then(res => res.json())
      .then(res => {
        let resp = {};
        resp[model.modelName + 's'] = res;
        return resp;
      });
  },
  findRecord(store, model, id) {
    return fetch(this.host + '/' + model.modelName + 's/' + id)
      .then(res => res.json())
      .then(res => {
        let resp = {};
        resp[model.modelName] = res;
        return resp;
      });
  },
  query(store, model, query) {
    return fetch(this.host + '/' + model.modelName + 's?' + stringify(query))
      .then(res => res.json())
      .then(res => {
        let resp = {};
        resp[model.modelName] = res;
        return resp;
      });
  }
});
*/
