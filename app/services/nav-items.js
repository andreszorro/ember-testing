import Ember from 'ember';

export default Ember.Service.extend({
  auth: Ember.inject.service(),
  getNav() {
    return new Promise(resolve => {
      window.setTimeout(() => resolve([
        {
          label: 'About',
          route: 'about'
        },
        {
          label: 'People',
          route: 'users'
        },
        {
          label: 'Profile',
          route: 'profile'
        }
      ]), 200);
    });
  }
});
