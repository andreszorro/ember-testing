import Ember from 'ember';

export default Ember.Service.extend({
  store: Ember.inject.service(),
  loggedUser: null,
  loginUser(user) {
    return this.get('store').queryRecord('user', {
        username: user.username
      })
      .then(usr => {
        if (usr) {
          this.set('loggedUser', usr);
        }
        return usr;
      })
      .catch(err => {
        this.set('loggedUser', null);
        throw err;
      });
  },
  logoutUser() {
    this.set('loggedUser', null);
  },
  isLogged() {
    return Boolean(this.get('loggedUser'));
  }
});
