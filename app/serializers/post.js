import DS from 'ember-data';

export default DS.LSSerializer.extend({
  attrs: {
    user: { embedded: 'always' }
  }
});
