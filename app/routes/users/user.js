import Ember from 'ember';

export default Ember.Route.extend({
  auth: Ember.inject.service(),
	model(params){
		return this.store.find('user', params.id);
	}
});
