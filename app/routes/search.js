import Ember from 'ember';

export default Ember.Route.extend({
  model(props) {
    let search = new RegExp(props.searchTerm, 'gi');
    return Promise.all([

      // Find All Posts
      this.store.findAll('post')
        .then(res => {
            res = res.filter(i =>
              search.test(i.get('body'))
            );

            return { postResults: res };
          }),
      // Find all Users
      this.store.findAll('user')
        .then(res => {
          res = res.filter(u =>
            search.test(u.get('username')) || search.test(u.get('name'))
          );

          return { userResults: res };
        })
    ]).then(promises => Object.assign(props, promises[0], promises[1]));
  }
});
