import Ember from 'ember';

export default Ember.Route.extend({
  auth: Ember.inject.service(),
  model() {
    return this.store.createRecord('user', {});
  },
  setupController(ctrl, model) {
    this._super(ctrl, model);
    ctrl.set('errors', {});
    ctrl.set('customErrorMessages', {
      regex: 'Match the regex, yo!'
    });
  },
  actions: {
    createUser(model) {
      let errors = model.validate();

      if (errors) {
        this.controller.set('errors', errors);
        return;
      }

      model.save().then(() => {
        this.get('auth').loginUser(model.toJSON());
        this.transitionTo('profile.user', model);
      });
    }
  }
});
