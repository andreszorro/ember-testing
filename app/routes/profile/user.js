import Ember from 'ember';

export default Ember.Route.extend({
  auth: Ember.inject.service(),
  model() {
    return this.get('auth').loggedUser;
  },
  actions: {
    logout() {
      this.get('auth').logoutUser();
      this.transitionTo('profile.login');
    }
  }
});
