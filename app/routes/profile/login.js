import Ember from 'ember';

export default Ember.Route.extend({
  auth: Ember.inject.service(),
  setupController(ctrl) {
    ctrl.set('model', {
      username: ''
    });
    ctrl.set('loginError', false);
  },
  actions: {
    login(model) {
      if (!model.username) {
        this.controller.set('loginError', 'Please write an user.');
        return;
      }
      this.controller.set('loginError', false);

      this.get('auth')
          .loginUser(model)
          .then(usr => {
            this.transitionTo('profile.user', usr);
          })
          .catch(() => {
            this.controller.set('loginError', `The user doesn't exist`);
          });
    }
  }
});
