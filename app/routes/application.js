import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    goToSearch(whereTo) {
      this.transitionTo('search', whereTo);
    }
  }
});
