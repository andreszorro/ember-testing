import Ember from 'ember';

export default Ember.Route.extend({
  auth: Ember.inject.service(),

  beforeModel(tr) {
    if (this.get('auth').isLogged()) {
      this.transitionTo('profile.user', this.get('auth').loggedUser);
    } else if (tr.targetName === 'profile.new') {
      this.transitionTo('profile.new');
    } else {
      this.transitionTo('profile.login');
    }
  },
  actions: {
    willTransition(tr) {
      if (/^profile/.test(tr.targetName)) {
        if (this.get('auth').isLogged()) {
          this.transitionTo('profile.user', this.get('auth').loggedUser);
        } else if (tr.targetName === 'profile.new') {
          this.transitionTo('profile.new');
        } else if (tr.targetName === 'profile.login' || tr.targetName === 'profile.index') {
          this.transitionTo('profile.login');
        }
      }
    }
  }

  //,
  // actions: {
  //   error() {
  //     this.transitionTo('profile.new');
  //   }
  // }
});
