export default function () {
  this.transition(
    this.fromRoute('index'),
    this.use('crossFade')
  );
}
