# Embertesting

This is an example Ember app for Training purposes. The idea is to mimick some common Front End scenarios in order to learn and solve problems in Ember's way. In order to learn, please make use of the already resolved scenarios and improve them, or create new problems in order to solve them.

Please use [this](http://jsonplaceholder.typicode.com/) example API to populate data. For images, [this](http://lorempixel.com/) is a good resource of random pictures. Also, other resources are welcome. 

Some suggested tasks are the following:

## Tasks (Suggested)
- Profile page
- Search Results page
- User information page
- Friends Page
- Comments box
- Posts and timeline
- Notifications bar 

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://www.ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

